    angular.
    module('meanApp').
    config(['$locationProvider', '$routeProvider',
      function config($locationProvider, $routeProvider) {
        $locationProvider.hashPrefix('!');

        $routeProvider.
        when('/', {
          //template: '<dashboard-view><dashboard-view>'
          template: '<create-todo><create-todo>'
        }).
        when('/todos', {
          template: '<list-todos><list-todos>'
        }).
        when('/todos/title/:todoId', {
          template: '<view-todo><view-todo>'
        }).
        when('/list', {
          template: '<list-todos><list-todos>'
        }).        
        when('/list/:findByCounty', {
          //template: '<view-todo><view-todo>'
          template: '<list-todos><list-todos>'
        }).
        when('/todos/create', {
          template: '<create-todo><create-todo>'
        }).
        when('/todos/:todoId', {
          template: '<view-todo><view-todo>'
        }).
        when('/todos/:todoId/edit', {
          template: '<edit-todo><edit-todo>'
        }).
        otherwise('/');
      }
    ]);