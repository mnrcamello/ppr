angular.module('core.todos').
    factory('Todos',['$resource',
        function($resource){
            return $resource('api/todos/:todoId', {
            todoId: '@_id'
            }, {
            update: {
                method: 'PUT'
            }
            });
        }
    ]);

    angular.module('core.todos').
    factory('findByCounty',['$resource',
        function($resource){
            return $resource('api/list/:findByCounty', {
                findByCounty: '@_id'
            }, {
            update: {
                method: 'PUT'
            }
            });
        }
    ]);

    angular.module('core.todos').
    factory('countyDropdown',['$resource',
        function($resource){
            return $resource('api/county/:countyDropdown', {
                countyDropdown: '@_id'
            }, {
            update: {
                method: 'PUT'
            }
            });
        }
    ]);

    angular.module('core.todos').
    factory('areaDropdown',['$resource',
        function($resource){
            return $resource('api/area/:areaDropdown', {
                areaDropdown: '@_id'
            }, {
            update: {
                method: 'PUT'
            }
            });
        }
    ]);

    angular.module('core.todos').
    factory('nationalMedian',['$resource',
        function($resource){
            return $resource('api/national/:nationalMedian', {
                nationalMedian: '@_id'
            }, {
            update: {
                method: 'PUT'
            }
            });
        }
    ]);

    angular.module('core.todos').
    factory('highestLowestValue',['$resource',
        function($resource){
            return $resource('api/highestlowest/:highestLowestValue', {
                highestLowestValue: '@_id'
            }, {
            update: {
                method: 'PUT'
            }
            });
        }
    ]);

    angular.module('core.todos').
    factory('averageValue',['$resource',
        function($resource){
            return $resource('api/getaverage/:averageValue', {
                averageValue: '@_id'
            }, {
            update: {
                method: 'PUT'
            }
            });
        }
    ]);

    angular.module('core.todos').
    factory('currentTotalSales',['$resource',
        function($resource){
            return $resource('api/totalsales/:currentTotalSales', {
                currentTotalSales: '@_id'
            }, {
            update: {
                method: 'PUT'
            }
            });
        }
    ]);
