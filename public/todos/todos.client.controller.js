//@TODO CHECK: I've removed '$scope' from the array of directive dependencies below,
//check that $scope does not introduce errors.

angular.module('todos').
controller('TodosCtrl', ['$routeParams', '$location', 'UserAuthentication', 'Todos', 'findByCounty', 'countyDropdown', 'areaDropdown', 'nationalMedian', 'highestLowestValue', 'averageValue', 'currentTotalSales',
    function ($routeParams, $location, UserAuthentication, Todos, findByCounty, countyDropdown, areaDropdown, nationalMedian, highestLowestValue, averageValue, currentTotalSales) {

        this.authentication = UserAuthentication;
        this.testVar = "Unit Test Successful!";
        this.otherTestVar = "Another Unit Test Successful!";

        if (this.authentication.user) {
            this.showFilters = false;
        } else {
            this.showFilters = true;
        }

        this.btnLogin = function () {
            location.href = '/login';
        };
        this.btnRegister = function () {
            location.href = '/register';
        };

        this.showGraph = function () {
                if (this.showFilters)
                    this.showFilters = false;
                else
                    this.showFilters = true;
            },

            this.upload = function () {
                var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.csv|.txt)$/;
                if (regex.test($("#fileUpload").val().toLowerCase())) {
                    if (typeof (FileReader) != "undefined") {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            var customers = new Array();
                            var rows = e.target.result.split("\n");
                            for (var i = 0; i < rows.length; i++) {
                                var cells = rows[i].split(",");
                                if (cells.length > 1) {
                                    var customer = {};
                                    customer.Date = cells[0];
                                    customer.Address = cells[1];
                                    customer.Area = cells[2];
                                    customer.Postal = cells[3];
                                    customer.County = cells[4];
                                    customer.Price = cells[5];
                                    customers.push(customer);
                                }
                            }
                            $("#dataJson").html('');
                            $("#dataJson").append(JSON.stringify(customers, null, 2));
                        }
                        reader.readAsText($("#fileUpload")[0].files[0]);
                    } else {
                        this.dataUploadError = 'This browser does not support HTML5';
                        this.uploadError = true;
                    }
                } else {
                    this.dataUploadError = "Please upload a valid CSV file."
                    this.uploadError = true;
                }
                this.uploadError = false;
            };

        let formatDateString = function (inputDate) {
            var date = new Date(inputDate);
            if (!isNaN(date.getTime())) {
                return date.getFullYear() + '-' + ("0" + (date.getMonth() + 1)).toString().slice(-2) + '-' + ("0" + date.getDate()).toString().slice(-2);
            }
            //return new Date(inputDate);
        };

        this.saveCsvData = function () {
            this.csvData = $('#dataJson').text();
            $('#confirmCsvSave').modal('hide')
            toastr.info('Processing. Do not close the browser', 'Ongoing');

            let items = JSON.parse(this.csvData);
            let title = this.title;
            items.forEach(function (item) {

                var todo = new Todos({
                    date: formatDateString(item.Date),
                    title: title,
                    address: item.Address,
                    area: item.Area,
                    postal: item.Postal,
                    county: item.County,
                    price: item.Price
                });

                todo.$save(function (response) {
                    $location.path('/');
                }, function (errorResponse) {
                    this.error = errorResponse.data.message;
                });
            });

            toastr.success('The process has been saved.', 'Success');
            /* setTimeout(function () {
                location.reload();
            }, 2000); */

            /* todo.$save(function (response) {
                //$location.path('todos/' + response._id);
                $location.path('/');
            }, function (errorResponse) {
                this.error = errorResponse.data.message;
            }); */

        };

        this.filterOptions = [
            /*{
                        value: 'range',
                        label: 'Range'
                    }, */
            {
                value: 'monthly',
                label: 'Monthly'
            }, {
                value: 'quarterly',
                label: 'Quarterly'
            }, {
                value: 'yearly',
                label: 'Yearly'
            }
        ]

        this.filterType = this.filterOptions[0].value;

        this.changeFilter = function (filterType) {
            //let filterType = $("#filterType").val().replace('string:', '');

            if (filterType == 'monthly') {
                $("#filterMonthly").removeClass("btn-green-filter");
                $("#filterMonthly").addClass("btn-green-filter-darker");
                $("#filterQuarterly").removeClass("btn-green-filter-darker");
                $("#filterQuarterly").addClass("btn-green-filter");
                $("#filterYearly").removeClass("btn-green-filter-darker");
                $("#filterYearly").addClass("btn-green-filter");

                if ($("#range").is(':visible')) { // Check for current tab opened
                    $("#range").fadeOut("slow", function () {
                        $("#monthly").fadeIn("slow", function () {}); // Show selected tab
                    });
                }
                if ($("#yearly").is(':visible')) {
                    $("#yearly").fadeOut("slow", function () {
                        $("#monthly").fadeIn("slow", function () {});
                    });
                }
                if ($("#quarterly").is(':visible')) {
                    $("#quarterly").fadeOut("slow", function () {
                        $("#monthly").fadeIn("slow", function () {});
                    });
                }
            }
            if (filterType == 'range') {
                if ($("#monthly").is(':visible')) {
                    $("#monthly").fadeOut("slow", function () {
                        $("#range").fadeIn("slow", function () {});
                    });
                }
                if ($("#yearly").is(':visible')) {
                    $("#yearly").fadeOut("slow", function () {
                        $("#range").fadeIn("slow", function () {});
                    });
                }
                if ($("#quarterly").is(':visible')) {
                    $("#quarterly").fadeOut("slow", function () {
                        $("#range").fadeIn("slow", function () {});
                    });
                }
            }
            if (filterType == 'yearly') {
                $("#filterYearly").removeClass("btn-green-filter");
                $("#filterYearly").addClass("btn-green-filter-darker");
                $("#filterMonthly").removeClass("btn-green-filter-darker");
                $("#filterMonthly").addClass("btn-green-filter");
                $("#filterQuarterly").removeClass("btn-green-filter-darker");
                $("#filterQuarterly").addClass("btn-green-filter");

                if ($("#range").is(':visible')) {
                    $("#range").fadeOut("slow", function () {
                        $("#yearly").fadeIn("slow", function () {});
                    });
                }
                if ($("#monthly").is(':visible')) {
                    $("#monthly").fadeOut("slow", function () {
                        $("#yearly").fadeIn("slow", function () {});
                    });
                }
                if ($("#quarterly").is(':visible')) {
                    $("#quarterly").fadeOut("slow", function () {
                        $("#yearly").fadeIn("slow", function () {});
                    });
                }
            }
            if (filterType == 'quarterly') {
                $("#filterQuarterly").removeClass("btn-green-filter");
                $("#filterQuarterly").addClass("btn-green-filter-darker");
                $("#filterMonthly").removeClass("btn-green-filter-darker");
                $("#filterMonthly").addClass("btn-green-filter");
                $("#filterYearly").removeClass("btn-green-filter-darker");
                $("#filterYearly").addClass("btn-green-filter");


                if ($("#range").is(':visible')) {
                    $("#range").fadeOut("slow", function () {
                        $("#quarterly").fadeIn("slow", function () {});
                    });
                }
                if ($("#monthly").is(':visible')) {
                    $("#monthly").fadeOut("slow", function () {
                        $("#quarterly").fadeIn("slow", function () {});
                    });
                }
                if ($("#yearly").is(':visible')) {
                    $("#yearly").fadeOut("slow", function () {
                        $("#quarterly").fadeIn("slow", function () {});
                    });
                }
            }
        };

        this.resetRangeFilter = function () {
            if ($("#variantRange option[value='']").length > 0) {
                $('#variantRange').val('');
            } else {
                $("#variantRange").prepend("<option value='' selected='selected'></option>");
            }
            if ($("#variantAreaRange option[value='']").length > 0) {
                $('#variantAreaRange').val('');
            } else {
                $("#variantAreaRange").prepend("<option value='' selected='selected'></option>");
            }
        };

        this.searchDataRange = function () {

            showlLoader();
            let startDate = $("#datetimepickerStart input").val();
            let endDate = $("#datetimepickerEnd input").val();
            let variant = $("#variantAreaRange").val().replace('string:', '');
            let variantArea = $("#variantArea").val() ? $("#variantArea").val().replace('string:', '') : 'null';

            let getParam = variant.trim() + '&' + variantArea + '&' + 'range' + '&' + startDate + '&' + endDate;


            //let getParam = 'Dublin&range&2020-01-01&2020-01-03';
            //let getParam = 'Kilkeny&range&2020-01-01&2020-01-03';

            this.todo = findByCounty.query({
                findByCounty: getParam
            }, function (response) {
                showlLoader();
                if (response) {
                    hideLoader();
                    let result = dataGraphs(response);
                    if (result == false) {
                        resultNull();
                    }
                }
            });

            $("#chartTitle").html(variant);

        };

        this.startMonth = [{
            value: '01',
            label: 'January'
        }, {
            value: '02',
            label: 'February'
        }, {
            value: '03',
            label: 'March'
        }, {
            value: '04',
            label: 'April'
        }, {
            value: '05',
            label: 'May'
        }, {
            value: '06',
            label: 'June'
        }, {
            value: '07',
            label: 'July'
        }, {
            value: '08',
            label: 'August'
        }, {
            value: '09',
            label: 'September'
        }, {
            value: '10',
            label: 'October'
        }, {
            value: '11',
            label: 'November'
        }, {
            value: '12',
            label: 'December'
        }]

        this.startMonthDefault = this.startMonth[0].value;

        this.endMonth = [{
            value: '01',
            label: 'January'
        }, {
            value: '02',
            label: 'February'
        }, {
            value: '03',
            label: 'March'
        }, {
            value: '04',
            label: 'April'
        }, {
            value: '05',
            label: 'May'
        }, {
            value: '06',
            label: 'June'
        }, {
            value: '07',
            label: 'July'
        }, {
            value: '08',
            label: 'August'
        }, {
            value: '09',
            label: 'September'
        }, {
            value: '10',
            label: 'October'
        }, {
            value: '11',
            label: 'November'
        }, {
            value: '12',
            label: 'December'
        }]

        this.endMonthDefault = this.endMonth[11].value;

        this.resetMonthlyFilter = function () {
            if ($("#variantMonthly option[value='']").length > 0) {
                $('#variantMonthly').val('');
            } else {
                $("#variantMonthly").prepend("<option value='' selected='selected'></option>");
            }
            if ($("#variantAreaMonthly option[value='']").length > 0) {
                $('#variantAreaMonthly').val('');
            } else {
                $("#variantAreaMonthly").prepend("<option value='' selected='selected'></option>");
            }
        };


        this.searchDataMonthly = function () {
            showlLoader();
            let variant = $("#variantMonthly").val().replace('string:', '');
            let startMonth = $("#startMonth").val().replace('string:', '');
            let endMonth = $("#endMonth").val().replace('string:', '');
            let variantArea = $("#variantAreaMonthly").val() ? $("#variantAreaMonthly").val().replace('string:', '') : '?';
            let yearList = $("#yearList").val().replace('string:', '');
            let vsComparison = $("input[name='monthlyYearComparison']:checked").val();
            let yearRange = [];
            let currYear;
            yearRange.push(parseInt(yearList));
            let yearListLoop = yearList;
            for (x = 0; x < vsComparison; x++) {
                yearListLoop = yearListLoop - 1;
                yearRange.push(yearListLoop);
            }
            let getParam = variant.trim() + '&' + variantArea + '&' + 'monthly' + '&' + startMonth + '&' + endMonth + '&' + yearList + '&' + vsComparison;

            this.todo = findByCounty.query({
                findByCounty: getParam
            }, function (response) {
                if (response) {
                    hideLoader();
                    let result = dataGraphsMonthly(response, parseInt(startMonth, 10) + '&' + parseInt(endMonth, 10), yearRange);
                    $("#chartTitle").html(variant);
                    if (result == false) {
                        resultNull();
                    }
                }
            });

        };

        this.yearList = [{
            value: '2017',
            label: '2017'
        }, {
            value: '2018',
            label: '2018'
        }, {
            value: '2019',
            label: '2019'
        }, {
            value: '2020',
            label: '2020'
        }];

        this.yearListDefault = this.yearList[3].value;

        this.startYear = [{
            value: '2017',
            label: '2017'
        }, {
            value: '2018',
            label: '2018'
        }, {
            value: '2019',
            label: '2019'
        }, {
            value: '2020',
            label: '2020'
        }];

        let yearListQuarter = [{
            value: '1',
            label: 'Q1'
        }, {
            value: '2',
            label: 'Q2'
        }, {
            value: '3',
            label: 'Q3'
        }, {
            value: '4',
            label: 'Q4'
        }];

        this.resetQuarterlyFilter = function () {
            if ($("#variantQuarterly option[value='']").length > 0) {
                $('#variantQuarterly').val('');
            } else {
                $("#variantQuarterly").prepend("<option value='' selected='selected'></option>");
            }
            if ($("#variantAreaQuarterly option[value='']").length > 0) {
                $('#variantAreaQuarterly').val('');
            } else {
                $("#variantAreaQuarterly").prepend("<option value='' selected='selected'></option>");
            }
        };

        this.searchDataQuarterly = function () {
            let variant = $("#variantQuarterly").val().replace('string:', '');
            let yearList = $("#yearList").val().replace('string:', '');
            let variantArea = $("#variantAreaQuarterly").val() ? $("#variantAreaQuarterly").val().replace('string:', '') : '?';
            let vsComparison = $("input[name='QuarterlyYearComparison']:checked").val();
            let yearRange = [];
            let currYear;
            yearRange.push(parseInt(yearList));
            let yearListLoop = yearList;
            for (x = 0; x < vsComparison; x++) {
                yearListLoop = yearListLoop - 1;
                yearRange.push(yearListLoop);
            }

            let getParam = variant.trim() + '&' + variantArea + '&' + 'quarterly' + '&' + yearList + '&' + vsComparison;


            this.todo = findByCounty.query({
                findByCounty: getParam
            }, function (response) {
                showlLoader();
                if (response) {
                    hideLoader();
                    let result = dataGraphsQuarterly(response, yearList, yearRange);
                    if (result == false) {
                        resultNull();
                    }
                }
            });

            $("#chartTitle").html(variant);
        };

        this.startYearDefault = this.startYear[2].value;

        this.endYear = [{
            value: '2017',
            label: '2017'
        }, {
            value: '2018',
            label: '2018'
        }, {
            value: '2019',
            label: '2019'
        }, {
            value: '2020',
            label: '2020'
        }];

        this.endYearDefault = this.endYear[3].value;

        this.resetYearlyFilter = function () {
            if ($("#variantYearly option[value='']").length > 0) {
                $('#variantYearly').val('');
            } else {
                $("#variantYearly").prepend("<option value='' selected='selected'></option>");
            }
            if ($("#variantAreaYearly option[value='']").length > 0) {
                $('#variantAreaYearly').val('');
            } else {
                $("#variantAreaYearly").prepend("<option value='' selected='selected'></option>");
            }
        };


        this.searchDataYearly = function () {
            let variant = $("#variantYearly").val().replace('string:', '');
            let startYear = $("#startYear").val().replace('string:', '');
            let endYear = $("#endYear").val().replace('string:', '');
            let variantArea = $("#variantAreaYearly").val() ? $("#variantAreaYearly").val().replace('string:', '') : '?';
            let getParam = variant.trim() + '&' + variantArea + '&' + 'yearly' + '&' + startYear + '&' + endYear;

            this.todo = findByCounty.query({
                findByCounty: getParam
            }, function (response) {
                showlLoader();
                if (response) {
                    hideLoader();
                    let result = dataGraphsYearly(response, startYear + '&' + endYear);
                    if (result == false) {
                        resultNull();
                    }
                }
            });

            $("#chartTitle").html(variant);
        };

        this.searchFilter = function () {

        }

        this.create = function () {
            var todo = new Todos({
                title: this.title,
                comment: this.comment
            });

            todo.$save(function (response) {
                $location.path('todos/' + response._id);
            }, function (errorResponse) {
                this.error = errorResponse.data.message;
            });
        };

        this.find = function () {
            this.todos = Todos.query();
        };

        let randomColor = function () {
            var num = Math.round(0xffffff * Math.random());
            var r = num >> 16;
            var g = num >> 8 & 255;
            var b = num & 255;
            return 'rgb(' + r + ', ' + g + ', ' + b + ',0.5)';
        }

        let fixedColor = function (ctr) {
            let position = ctr;
            colors = ['#62A904', '#212426', '#ff9e00'];

            return colors[position];
        }

        let getComputeIncrease = function (curr, prev) {
            let current = Array.isArray(curr) == true ? curr.reduce((a, b) => a + b, 0) : curr;
            let previous = Array.isArray(prev) == true ? prev.reduce((a, b) => a + b, 0) : prev;
            let percentIncrease = ((current - previous) / previous) * 100;

            return percentIncrease > 0 ? '↑' + Math.abs(percentIncrease.toFixed(2)) : '↓' + Math.abs(percentIncrease.toFixed(2));
        };

        let getSubtractedValue = function (curr, prev) {

            let current = Array.isArray(curr) == true ? curr.reduce((a, b) => a + b, 0) : curr;
            let previous = Array.isArray(prev) == true ? prev.reduce((a, b) => a + b, 0) : prev;
            let subtractedValue = current - previous;

            return subtractedValue;
        };


        let dashboardMedianDataPrice = function (priceData, lastPriceData, graphFilterType_, totalCountList) {

            loadHighestLowestValue();
            getAverageValue(setPriceComparisonPercentage);
            getTotalSales();

            let graphFilterType = graphFilterType_;
            let totalPriceData = graphFilterType == 'years' ? priceData : priceData.reduce((a, b) => a + b, 0); // Years value doesn't need to be added
            let totalLastPriceData = graphFilterType == 'years' ? lastPriceData : lastPriceData.reduce((a, b) => a + b, 0); // Years value doesn't need to be added

            // for average price summary
            if (totalPriceData > 0) {
                let dataDashboardMedian = priceData;

                let highestValuePrice = Array.isArray(dataDashboardMedian) == true ? Math.ceil(Math.max.apply(null, dataDashboardMedian)) : 0;
                let lowestValuePrice = Array.isArray(dataDashboardMedian) == true ? Math.ceil(Math.min.apply(null, dataDashboardMedian)) : 0;
                //let averageValuePrice = Array.isArray(dataDashboardMedian) == true ? Math.ceil((highestValuePrice + lowestValuePrice) / dataDashboardMedian.length) : 0;
                //let averageValuePrice = Array.isArray(dataDashboardMedian) == true ? Math.ceil((totalPriceData) / dataDashboardMedian.length) : 0;
                let averageValuePrice = Array.isArray(dataDashboardMedian) == true ? Math.ceil((totalPriceData) / totalCountList) : 0;

                let averagePriceValue = '';
                let highLowPriceValue = '';
                highLowPriceValue += `<div class="box-summary">`;
                //averagePriceValue += `<h3 class="medium-heading">€ ${averageValuePrice.toLocaleString()}</h3>`; OLD Logic
                averagePriceValue += `<h3 class="medium-heading">€ <span id='set-median-price'></span></h3>`;
                averagePriceValue += `<div class="text-left">.....................................</div>`;

                // highLowPriceValue += `<div class='box-summary-inner'>`;
                highLowPriceValue += `<div>Highest Value:</div>`;
                //highLowPriceValue += `<div class="key-numbers-bold">€ ${highestValuePrice.toLocaleString()}</div>`;
                highLowPriceValue += `<div class="key-numbers-bold">€ <span id="highest-value"></span></div>`; // To be fixed
                highLowPriceValue += `<div class="text-center">.....................................</div>`;
                highLowPriceValue += `<div>Lowest Value: </div>`;
                highLowPriceValue += `<div class="key-numbers-bold">€ <span id="lowest-value"></span></div>`;
                highLowPriceValue += `<div class="text-slant">During selected time period</div>`;
                //highLowPriceValue += `</div>`;
                highLowPriceValue += `</div>`;

                $('#average-price-value').html(averagePriceValue);
                $('#high-low-price-value').html(highLowPriceValue);
                loadNationalMedian(); // asign national median

            }
            //for price comparison summary
            if (totalLastPriceData > 0) { // Should have comparison data for previous year
                let lastPriceValue = lastPriceData;
                let percentIncrease = '';
                let subtractedValue = '';

                let priceComparisonSummary = '';
                let finalSubtractedValue = Math.ceil(subtractedValue);

                priceComparisonSummary += `<div>`;
                priceComparisonSummary += `<h3><span id='set-percentage-increase-value'></span>%</h3>`;
                priceComparisonSummary += `<div class="text-left">.....................................</div>`;
                priceComparisonSummary += `<div>€ <span id="set-more-expensive"></span> More Expensive</div>`;
                priceComparisonSummary += `<div>Than in previous ${lastPriceValue.length} ${graphFilterType} period</div>`;
                priceComparisonSummary += `<div>(€ <span id="set-previous-median"></span>)</div>`;
                priceComparisonSummary += `<div class="text-slant">During selected time period</div>`;
                priceComparisonSummary += `</div>`;

                let whyMedianText = '';
                whyMedianText += `<div class="box-summary ">`;
                whyMedianText += `<div class="text-left"><div><p class='text-bold'>Why do we use Median and not Average?</p></div> <p class="text-small"><b>Median</b> is determined by ranking the data from largest to smallest, and then identifying smaller than the central number. </p> <p class="text-small">This provides a more accurate reflection of property prices, because <b>average</b> can be skewed by several very expensive property sales e.g large pieces of land or the sale of an entire block of units and therefore misrepresents the middle price point for a county or area.</p></div>`;
                whyMedianText += `</div>`;
                $('#why-median-text').html(whyMedianText);

                let numberOfSales = '';
                numberOfSales += `<div class="box-summary">`;
                numberOfSales += `<div class='box-summary-inner-no-line'>`;
                numberOfSales += `<div>Number of Sales: </div>`;
                //numberOfSales += `<div class="key-numbers-bold">${totalCountList}</div>`;
                numberOfSales += `<div class="key-numbers-bold"><span id='set-current-total-sales'></span></div>`;

                numberOfSales += `<div class="text-center">.....................................</div>`;
                numberOfSales += `<div>National Median:</div>`;
                numberOfSales += `<div class="key-numbers-bold"> € <span id='national-median-value'></span></div>`;
                numberOfSales += `<div class="text-slant">During selected time period</div>`;
                numberOfSales += `</div>`;
                numberOfSales += `</div>`;

                $('#price-comparison-summary').html(priceComparisonSummary);
                $('#total-sales-summary').html(numberOfSales);
            } else {
                $('#price-comparison-summary').html('<div>No Data to Compare.</div>');
            }

        };

        let getIncreasePercentageData = function (priceCurrent, pricePrevious) {
            increaseValue = (priceCurrent / pricePrevious);

            return isNaN(increaseValue) ? '0.00' : increaseValue.toFixed(2);
        };

        let dataGraphsQuarterly = function (responseRaw, year, yearRange) {
            let graphFilterType = 'quarters';
            let response = [];
            let result = false;
            let title = [];
            let price = [];
            let priceRaw = [];
            let color = [];
            let dateData = [];
            let dateDataRaw = [];
            let lastPriceRaw = [];
            let lastDateDataRaw = [];
            let lastTwoPriceRaw = [];
            let lastTwoDateDataRaw = [];
            let monthSelection = [];
            let lastPrice = [];
            let lastTwoPrice = [];
            let i;
            let responseIndex = 0;
            let currentData = [];
            let LastData = [];
            let lastTwoData = [];
            let quarterlyAllData = [];
            let quarterlyDataSet = [];
            let firstQ = [1, 2, 3];
            let secondQ = [4, 5, 6];
            let thirdQ = [7, 8, 9];
            let fourthQ = [10, 11, 12];
            let amountOrder = [];
            let current = 1;
            let firstInc = 0;
            let secondInc = 0;
            let thirdInc = 0;
            let fourthInc = 0;
            let position = 0;
            let perQuarterCount = [];
            let totalCountOfList = [];
            let totalSumOfAllList = [];

            responseRaw.forEach(function (item) {
                result = true;
                priceRaw[item._id.month] = item.totalAmount;
                amountOrder.push({
                    month: item._id.month,
                    year: item._id.year,
                    count: item.count,
                    amount: item.totalAmount
                });
                //color.push(randomColor());
            });

            yearListQuarter.forEach(function (quarter) {
                dateData.push((quarter.label));
                price.push(0); // Set default value for price
                perQuarterCount.push(0);
            });

            yearRange.forEach(function (currYear) {

                amountOrder.forEach(function (amountOrderData) {
                    if (responseIndex == 0) {
                        if (amountOrderData.year == currYear) {
                            if (firstQ.includes(amountOrderData.month)) {
                                if (amountOrderData.amount) {
                                    totalCountOfList.push(amountOrderData.count); // Get total count of list for current year
                                    firstInc++;
                                    price[0] += amountOrderData.amount;
                                    //price[0] = price[0] / amountOrderData.count;
                                    perQuarterCount[0] += amountOrderData.count;

                                } else {
                                    price[0] += 0;
                                }
                            } else if (secondQ.includes(amountOrderData.month)) {
                                if (amountOrderData.amount) {
                                    totalCountOfList.push(amountOrderData.count); // Get total count of list for current year
                                    secondInc++;
                                    price[1] += amountOrderData.amount;
                                    //price[1] = price[1] / amountOrderData.count;
                                    perQuarterCount[1] += amountOrderData.count;
                                } else {
                                    price[1] += 0;
                                }
                            } else if (thirdQ.includes(amountOrderData.month)) {
                                if (amountOrderData.amount) {
                                    totalCountOfList.push(amountOrderData.count); // Get total count of list for current year
                                    thirdInc++;
                                    price[2] += amountOrderData.amount;
                                    //price[2] = price[2] / amountOrderData.count;
                                    perQuarterCount[2] += amountOrderData.count;
                                } else {
                                    price[2] += 0;
                                }
                            } else if (fourthQ.includes(amountOrderData.month)) {
                                if (amountOrderData.amount) {
                                    totalCountOfList.push(amountOrderData.count); // Get total count of list for current year
                                    fourthInc++;
                                    price[3] += amountOrderData.amount;
                                    //price[3] = price[3] / amountOrderData.count;
                                    perQuarterCount[3] += amountOrderData.count;
                                } else {
                                    price[3] += 0;
                                }
                            } else {
                                price[current] += 0;
                            }
                            current++;
                        }
                    }
                });

                quarterlyAllData.push((price[0] / perQuarterCount[0]));
                quarterlyAllData.push((price[1] / perQuarterCount[1]));
                quarterlyAllData.push((price[2] / perQuarterCount[2]));
                quarterlyAllData.push((price[3] / perQuarterCount[3]));
                totalSumOfAllList.push(price[0]);
                totalSumOfAllList.push(price[1]);
                totalSumOfAllList.push(price[2]);
                totalSumOfAllList.push(price[3]);
                price[0] = 0; // Reset for each year
                price[1] = 0;
                price[2] = 0;
                price[3] = 0;
                perQuarterCount[0] = 0;
                perQuarterCount[1] = 0;
                perQuarterCount[2] = 0;
                perQuarterCount[3] = 0;

            });

            if (quarterlyAllData.length >= 12) {
                //let color = randomColor();
                let color = fixedColor(position);
                quarterlyDataSet.push({
                    label: yearRange[2] + " Quarterly Median Price",
                    backgroundColor: color,
                    borderColor: color,
                    pointBorderColor: color,
                    data: quarterlyAllData.slice(8, 12),
                    color: color,
                    fill: false,
                    lineTension: false,
                    responsive: true
                });
                position++;
            }

            if (quarterlyAllData.length >= 8) {
                //let color = randomColor();
                let color = fixedColor(position);
                quarterlyDataSet.push({
                    label: yearRange[1] + " Quarterly Median Price",
                    backgroundColor: color,
                    borderColor: color,
                    pointBorderColor: color,
                    data: quarterlyAllData.slice(4, 8),
                    color: color,
                    fill: false,
                    lineTension: false,
                    responsive: true
                });
                position++;
            }

            if (quarterlyAllData.length >= 4) {
                //let color = randomColor();
                let color = fixedColor(position);
                quarterlyDataSet.push({
                    label: yearRange[0] + " Quarterly Median Price",
                    backgroundColor: color,
                    borderColor: color,
                    pointBorderColor: color,
                    data: quarterlyAllData.slice(0, 4),
                    color: color,
                    fill: false,
                    lineTension: false,
                    responsive: true
                });
                position++;
            }

            dashboardMedianDataPrice(totalSumOfAllList.slice(0, 4), totalSumOfAllList.slice(4, 8), graphFilterType, totalCountOfList.reduce((a, b) => a + b, 0));

            if (result) {
                let config = {
                    type: 'line',
                    data: {
                        labels: dateData,
                        datasets: quarterlyDataSet
                    },
                    options: {
                        scales: {
                            xAxes: [{
                                stacked: false
                            }],
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                },
                                stacked: false
                            }]
                        },
                        legend: {
                            display: false
                        },
                        title: {
                            display: true,
                            text: `Quarterly Median Price`
                        },
                        tooltips: {
                            callbacks: {
                                label: function (tooltipItem, data) {
                                    return data.datasets[tooltipItem.datasetIndex].label + ': €' + tooltipItem.yLabel.toFixed(2);
                                },
                                afterLabel: function (tooltipItem, data) {
                                    let dataCurrent = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                                    let dataPrevious = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index - 1];

                                    var percentage = isNaN((dataCurrent - dataPrevious) / dataPrevious) ? 0 : (dataCurrent - dataPrevious) / dataPrevious;

                                    //return "Increase: % " + percentage.toFixed(2);
                                    return "Increase: % " + percentage.toFixed(2);
                                }
                            }
                        },
                        showScale: false,
                        bezierCurve: false,
                    }
                }
                //var myLineChart = new Chart(ctx, config);
                if (typeof Graph === "undefined") {
                    window.Graph = new Chart(ctx, config);
                } else {
                    //updating with new chart data
                    window.Graph.config = config;
                    //redraw the chart
                    window.Graph.update();
                }
            } else {
                return false;
            }
        }

        let dataGraphsMonthly = function (responseRaw, months, yearRange) {

            let graphFilterType = 'months'
            let response = [];
            let monthRange = months.split("&");
            let result = false;
            let title = [];
            let price = [];
            let priceRaw = [];
            let priceRawTotal = [];
            let color = [];
            let dateData = [];
            let increaseData = [];
            let dateDataRaw = [];
            let lastPriceRaw = [];
            let lastPriceRawTotal = [];
            let lastDateDataRaw = [];
            let lastTwoPriceRaw = [];
            let lastTwoDateDataRaw = [];
            let lastTwoPriceRawTotal = [];
            let monthSelection = [];
            let lastPrice = [];
            let lastTwoPrice = [];
            let i;
            let responseIndex = 0;
            let currentData = [];
            let LastData = [];
            let lastTwoData = [];
            let monthlyDataSet = [];
            let increaseValue;
            let position = 0;
            let priceTotal = [];
            let lastPriceTotal = [];
            let lastTwoPriceTotal = [];
            let totalCountOfList = [];
            let totalCountOfListLastYear = [];

            yearRange.forEach(function (currYear) {
                responseRaw.forEach(function (item) {

                    if (responseIndex == 0) {
                        if (item._id.year == currYear) {
                            totalCountOfList.push(item.count); // Get total count of list for current year
                            currentData.push({
                                month: item._id.month,
                                year: item._id.year,
                                avgQuantity: item.avgQuantity,
                                totalAmount: item.totalAmount,
                            });
                        }
                    }
                    if (responseIndex == 1) {
                        if (item._id.year == currYear) {
                            LastData.push({
                                month: item._id.month,
                                year: item._id.year,
                                avgQuantity: item.avgQuantity,
                                totalAmount: item.totalAmount,
                            });
                        }
                    }
                    if (responseIndex == 2) {
                        if (item._id.year == currYear) {
                            lastTwoData.push({
                                month: item._id.month,
                                year: item._id.year,
                                avgQuantity: item.avgQuantity,
                                totalAmount: item.totalAmount,
                            });
                        }
                    }
                });
                responseIndex++;
            });

            if (currentData.length != 0) {
                currentData.forEach(function (item) {
                    result = true;
                    priceRaw[item.month] = item.avgQuantity;
                    priceRawTotal[item.month] = item.totalAmount;
                    dateDataRaw.push((item.month - 1));
                });
            }

            if (LastData.length != 0) {
                LastData.forEach(function (item) {
                    result = true;
                    lastPriceRaw[item.month] = item.avgQuantity;
                    lastPriceRawTotal[item.month] = item.totalAmount;
                    lastDateDataRaw.push((item.month - 1));
                });
            }
            if (lastTwoData.length != 0) {
                lastTwoData.forEach(function (item) {
                    result = true;
                    lastTwoPriceRaw[item.month] = item.avgQuantity;
                    lastTwoPriceRawTotal[item.month] = item.totalAmount;
                    lastTwoDateDataRaw.push((item.month - 1));
                });
            }

            // Assign length of month selected
            var monthLength = parseInt(monthRange[1]) - parseInt(monthRange[0]);
            monthLength = monthLength + parseInt(monthRange[0]); // The total length of months should always be 12
            for (i = parseInt(monthRange[0]); i <= parseInt(monthRange[1]); i++) {
                dateData.push(this.endMonth[i - 1].label) // Display all selected months
                increaseValue = getIncreasePercentageData(priceRaw[i], priceRaw[i - 1]);
                increaseData.push(`${increaseValue} % increase in ${this.endMonth[i - 1].label}`);
                //if (dateDataRaw.includes(i - 1)) { // Iterate with index key
                if (i - 1 <= monthLength) {
                    price.push(priceRaw[i]); // Insert amount from csv entries
                    lastPrice.push(lastPriceRaw[i]);
                    lastTwoPrice.push(lastTwoPriceRaw[i]);
                    priceTotal.push(priceRawTotal[i]);
                    lastPriceTotal.push(lastPriceRawTotal[i]);
                    lastTwoPriceTotal.push(lastTwoPriceRawTotal[i]);
                } else {
                    price.push('0'); // Insert zero amount from none null csv entries
                    lastPrice.push('0');
                    lastTwoPrice.push('0');
                    priceTotal.push('0');
                    lastPriceTotal.push('0');
                    lastTwoPriceTotal.push('0');
                }
                increaseValue = '';
            }

            if (lastTwoData.length != 0) {
                //let color = randomColor();
                let color = fixedColor(position);
                monthlyDataSet.push({
                    label: yearRange[2] + " Monthly Median Price",
                    backgroundColor: color,
                    borderColor: color,
                    pointBorderColor: color,
                    fill: false,
                    responsive: true,
                    maintainAspectRatio: false,
                    lineTension: false,
                    data: lastTwoPrice.map(v => v === undefined ? 0 : v) // Change undefined value to zero
                });
                position++;
            }
            if (LastData.length != 0) {
                //let color = randomColor();
                let color = fixedColor(position);
                monthlyDataSet.push({
                    label: yearRange[1] + " Monthly Median Price",
                    backgroundColor: color,
                    borderColor: color,
                    pointBorderColor: color,
                    fill: false,
                    responsive: true,
                    maintainAspectRatio: false,
                    lineTension: false,
                    data: lastPrice.map(v => v === undefined ? 0 : v)
                });
                position++;
            }

            if (currentData.length != 0) {
                //let color = randomColor();
                let color = fixedColor(position);
                monthlyDataSet.push({
                    label: yearRange[0] + " Monthly Median Price",
                    backgroundColor: color,
                    borderColor: color,
                    pointBorderColor: color,
                    fill: false,
                    responsive: true,
                    maintainAspectRatio: false,
                    lineTension: false,
                    data: price.map(v => v === undefined ? 0 : v)
                });
                position++;
            }

            var toolTipCounter = 0;


            dashboardMedianDataPrice(priceTotal.map(v => v === undefined ? 0 : v), lastPriceTotal.map(v => v === undefined ? 0 : v), graphFilterType, totalCountOfList.reduce((a, b) => a + b, 0));

            if (result) {
                let config = {
                    type: 'line',
                    data: {
                        labels: dateData,
                        datasets: monthlyDataSet
                    },
                    /* data: {
                            labels: ["Chocolate", "Vanilla", "Strawberry"],
                            datasets: [
                                {
                                    label: "Blue",
                                    fillColor: "blue",
                                    data: [3,7,0]
                                },
                                {
                                    label: "Red",
                                    fillColor: "red",
                                    data: [4,3,5]
                                },
                                {
                                    label: "Green",
                                    fillColor: "green",
                                    data: [7,2,6]
                                }
                            ]
                    }, */
                    options: {
                        scales: {
                            xAxes: [{
                                stacked: false
                            }],
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    /* callback: function(value, index, values) {
                                        return '$' + value;
                                    } */
                                },
                                stacked: false
                            }]
                        },
                        title: {
                            display: true,
                            text: `Monthly Price Movement`
                        },
                        tooltips: {
                            callbacks: {
                                label: function (tooltipItem, data) {
                                    return data.datasets[tooltipItem.datasetIndex].label + ': €' + tooltipItem.yLabel.toFixed(2);
                                },
                                afterLabel: function (tooltipItem, data) {
                                    let dataCurrent = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                                    let dataPrevious = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index - 1];

                                    var percentage = isNaN((dataCurrent - dataPrevious) / dataPrevious) ? 0 : (dataCurrent - dataPrevious) / dataPrevious;

                                    //return "Increase: % " + percentage.toFixed(2);
                                    return "Increase: % " + percentage.toFixed(2);
                                }
                                /*afterLabel: function(tooltipItem, data){
                                     var total = 0;
                                     for (i = 0; i < data.datasets.length; i++) {
                                         if(i == 0){
                                             total = data.datasets[i].data[tooltipItem.datasetIndex];
                                         } else {
                                             total = data.datasets[toolTipCounter].data[tooltipItem.datasetIndex];
                                         }
                                         toolTipCounter++;
                                     }
                                     var percentage = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] / total;
                                     var percentage = percentage.toFixed(2); // Trim decimal part
                                     return "Percentage: %" + toolTipCounter;
                                   } */
                            }
                        },
                        responsive: false,
                        maintainAspectRatio: true,
                        showScale: false,

                    }
                }
                //var myLineChart = new Chart(ctx, config);
                if (typeof Graph === "undefined") {
                    window.Graph = new Chart(ctx, config);
                } else {
                    //updating with new chart data
                    window.Graph.config = config;
                    //redraw the chart
                    window.Graph.update();
                }
            } else {
                return false;
            }
        };

        let dataGraphsYearly = function (response, years) {
            let graphFilterType = 'years';
            let yearRange = years.split("&");
            let result = false;
            let title = [];
            let price = [];
            let color = [];
            let priceRaw = [];
            let dateData = [];
            let dateDataRaw = [];
            let i;
            let position = 0;

            response.forEach(function (item) {
                result = true;
                //priceRaw[item._id.year] = item.totalAmount;
                priceRaw[item._id.year] = item.avgQuantity;
                dateDataRaw.push(item._id.year);
                color.push(fixedColor(position));
                position++;
            });

            for (i = yearRange[0]; i <= yearRange[1]; i++) { // Get selected year range
                dateData.push(i) // Display all selected years
                if (typeof priceRaw[i] === 'undefined') {
                    price.push(0); // Insert zero amount from none null csv entries
                } else {
                    price.push(priceRaw[i]); // Insert amount from csv entries
                }
            }
            price = price.map(v => v === undefined ? 0 : v);

            dashboardMedianDataPrice(price[0], price[1], graphFilterType);

            if (result) {
                let config = {
                    type: 'line',
                    data: {
                        labels: dateData,
                        datasets: [{
                            label: "Yearly Median Price",
                            backgroundColor: color,
                            borderColor: color,
                            pointBorderColor: color,
                            data: price,
                            fill: false,
                            lineTension: false,
                        }]
                    },
                    options: {
                        scales: {
                            xAxes: [{
                                stacked: true,
                            }],
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                },
                                stacked: false
                            }]
                        },
                        legend: {
                            display: false
                        },
                        title: {
                            display: true,
                            text: `Yearly Price Movement`
                        },
                        tooltips: {
                            callbacks: {
                                label: function (tooltipItem, data) {
                                    return data.datasets[tooltipItem.datasetIndex].label + ': €' + tooltipItem.yLabel.toFixed(2);
                                },
                                afterLabel: function (tooltipItem, data) {
                                    let dataCurrent = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                                    let dataPrevious = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index - 1];

                                    var percentage = isNaN((dataCurrent - dataPrevious) / dataPrevious) ? 0 : (dataCurrent - dataPrevious) / dataPrevious;

                                    //return "Increase: % " + percentage.toFixed(2);
                                    return "Increase: % " + percentage.toFixed(2);
                                }
                            }
                        },
                        showScale: false,
                        bezierCurve: false,
                    }
                }
                //var myLineChart = new Chart(ctx, config);
                if (typeof Graph === "undefined") {
                    window.Graph = new Chart(ctx, config);
                } else {
                    //updating with new chart data
                    window.Graph.config = config;
                    //redraw the chart
                    window.Graph.update();
                }
            } else {
                return false;
            }
        };

        let dataGraphsDefault = function () {
            let config = {
                type: 'line',
                data: {
                    labels: ["No Data"],
                    datasets: [{
                        label: "No Data",
                        data: [0]
                    }, ]
                },
                options: {
                    scales: {
                        xAxes: [{
                            stacked: true,
                            ticks: {
                                fontSize: 14
                            },
                        }],
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                fontSize: 14,
                                userCallback: function (value, index, values) {
                                    // Convert the number to a string and splite the string every 3 charaters from the end
                                    value = value.toString();
                                    value = value.split(/(?=(?:...)*$)/);
                                    value = value.join(',');
                                    return '€ ' + value;
                                }
                            },
                            stacked: false
                        }]
                    },
                    legend: {
                        display: true,
                        position: 'bottom',
                        labels: {
                            fontColor: "#686868",
                        }
                    },
                    title: {
                        display: true,
                        //text: `Price Movement`
                    },
                    tooltips: {
                        titleFontSize: 14,
                        bodyFontSize: 14,
                        callbacks: {
                            label: function (tooltipItem, data) {
                                let totalCurrent = Math.ceil(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index]);

                                return data.datasets[tooltipItem.datasetIndex].label + ': € ' + totalCurrent.toLocaleString();
                            },
                            afterLabel: function (tooltipItem, data) {
                                let dataCurrent = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                                let dataPrevious = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index - 1];

                                var percentage = isNaN((dataCurrent - dataPrevious) / dataPrevious) ? 0 : (dataCurrent - dataPrevious) / dataPrevious;
                                if (dataPrevious == 0) {
                                    return "Increase: - ";
                                } else {
                                    return "Increase: " + percentage.toFixed(2) + " %";
                                }

                            }
                            /*afterLabel: function(tooltipItem, data){
                                 var total = 0;
                                 for (i = 0; i < data.datasets.length; i++) {
                                     if(i == 0){
                                         total = data.datasets[i].data[tooltipItem.datasetIndex];
                                     } else {
                                         total = data.datasets[toolTipCounter].data[tooltipItem.datasetIndex];
                                     }
                                     toolTipCounter++;
                                 }
                                 var percentage = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index] / total;
                                 var percentage = percentage.toFixed(2); // Trim decimal part
                                 return "Percentage: %" + toolTipCounter;
                               } */
                        }
                    },
                    responsive: false,
                    maintainAspectRatio: true,
                    showScale: false,

                }
            }
            //var myLineChart = new Chart(ctx, config);
            if (typeof Graph === "undefined") {
                window.Graph = new Chart(ctx, config);
            } else {
                //updating with new chart data
                window.Graph.config = config;
                //redraw the chart
                window.Graph.update();
            }
        };


        let dataGraphs = function (response) {

            let result = false;
            let title = [];
            let price = [];
            let color = [];
            let dateData = [];
            response.forEach(function (item) {
                result = true;
                title.push(item.county);
                price.push(item.price);
                let dateResult = new Date(item.date);
                dateData.push(dateResult.toLocaleDateString("en-US", {
                    year: 'numeric',
                    month: 'long',
                    day: 'numeric'
                }));
                color.push(randomColor());
            });
            if (result) {
                let config = {
                    type: 'bar',
                    data: {
                        labels: dateData,
                        datasets: [{
                            label: "Period Price",
                            backgroundColor: ["#3e95cd", "#8e5ea2"],
                            data: price
                        }]
                    },
                    options: {
                        scales: {
                            xAxes: [{
                                stacked: true
                            }],
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                },
                                stacked: false
                            }]
                        },
                        legend: {
                            display: false
                        },
                        title: {
                            display: true,
                            text: `Periodic Price Movement`
                        },
                        /* tooltips: {
                            callbacks: {
                                title: function (t, d) {
                                    return `${title}`;
                                }
                            }
                        } */
                    }
                }
                //var myLineChart = new Chart(ctx, config);
                if (typeof Graph === "undefined") {
                    window.Graph = new Chart(ctx, config);
                } else {
                    //updating with new chart data
                    window.Graph.config = config;
                    //redraw the chart
                    window.Graph.update();
                }
            } else {
                return false;
            }
        };

        let hideLoader = function () {
            $("#dataloader").hide();
            $("#chartContainer").show();

            this.dataloader = false;
        };

        let resultNull = function () {
            $("#dataloader").hide();
            $("#chartContainer").hide();
            $("#noResult").html('No Result')
            this.dataloader = false;
        };

        let showlLoader = function () {
            $("#dataloader").show();
            $("#chartContainer").hide();
            $("#noResult").html('');
            this.dataloader = true;
        };

        this.listAll = function () {
            this.dataloader = true;
            this.todos = findByCounty.query({}, function (response) {
                if (response) {
                    hideLoader();
                    dataGraphs(response);
                }
            });
        };

        this.countyDropdowns = function () {
            this.counties = countyDropdown.query({
                countyDropdown: 'all'
            });
        };

        let loadNationalMedian = function () {
            this.nationalMedian = nationalMedian.query({
                nationalMedian: 'all'
            }, function (response) {
                if (response) {
                    let selectedYear = $("#yearList").val().replace('string:', '');
                    response.forEach(function (item) {
                        if (item._id.year == selectedYear) {
                            let medianTotal = item.totalAmount;
                            let medianTotalCount = item.count;
                            let nationalAverageValue = Math.ceil(medianTotal / medianTotalCount);
                            $('#national-median-value').html(nationalAverageValue.toLocaleString());
                        }
                    });
                }
            });
        };

        let loadHighestLowestValue = function () {
            let variant = $("#variantMonthly").val().replace('string:', '');
            let variantArea = $("#variantAreaMonthly").val() ? $("#variantAreaMonthly").val().replace('string:', '') : '?';
            let startMonth = $("#startMonth").val().replace('string:', '');
            let endMonth = $("#endMonth").val().replace('string:', '');
            let yearList = $("#yearList").val().replace('string:', '');
            let vsComparison = $("input[name='monthlyYearComparison']:checked").val();

            let getParam = variant.trim() + '&' + variantArea + '&' + 'monthly' + '&' + startMonth + '&' + endMonth + '&' + yearList + '&' + vsComparison;

            this.highestLowestValue = highestLowestValue.query({
                highestLowestValue: getParam
            }, function (response) {
                if (response.length != 0) {
                    response.forEach(function (item) {
                        if (item._id.year == yearList) {
                            $('#highest-value').html(Math.ceil(item.maxPrice).toLocaleString());
                            $('#lowest-value').html(Math.ceil(item.minPrice).toLocaleString());
                        }
                    });
                } else {
                    $('#highest-value').html('0');
                    $('#lowest-value').html('0');
                }


            });
        };

        let getAverageValue = function (callback) {
            let variant = $("#variantMonthly").val().replace('string:', '');
            let variantArea = $("#variantAreaMonthly").val() ? $("#variantAreaMonthly").val().replace('string:', '') : '?';
            let startMonth = $("#startMonth").val().replace('string:', '');
            let endMonth = $("#endMonth").val().replace('string:', '');
            let yearList = $("#yearList").val().replace('string:', '');
            let vsComparison = $("input[name='monthlyYearComparison']:checked").val();
            let yearListNow = yearList;
            let medianPrice = 0;
            medianPriceGlobal = 0;
            let medianPricePrevious = 0;
            medianPricePreviousGlobal = 0;

            for (i = 0; i <= vsComparison; i++) { // Check vsComparison value if last year is chosen
                yearList = yearList - i;

                let getParam = variant.trim() + '&' + variantArea + '&' + 'monthly' + '&' + startMonth + '&' + endMonth + '&' + yearList + '&' + vsComparison;

                this.averageValue = averageValue.query({
                    averageValue: getParam
                }, function (response) {

                    response.forEach(function (item) {
                        if (item._id.year == yearListNow) { // Current Year
                            medianPrice = Math.ceil(item.totalAmount / item.count).toLocaleString();
                            medianPriceGlobal = Math.ceil(item.totalAmount / item.count);

                        } else if (item._id.year == (yearListNow - 1)) { // Last Year
                            medianPricePrevious = Math.ceil(item.totalAmount / item.count).toLocaleString();
                            medianPricePreviousGlobal = Math.ceil(item.totalAmount / item.count);

                        } else {}
                        $('#set-median-price').html(medianPrice);
                        $('#set-previous-median').html(medianPricePrevious);
                    });
                    callback();
                });
            }

        };

        let setPriceComparisonPercentage = function () {
            if (typeof medianPricePreviousGlobal !== 'undefined') { // Set every variable first from getAverageValue function
                let curentVal = medianPriceGlobal; // current
                let previousVal = medianPricePreviousGlobal; // previous
                let percentIncrease = ((curentVal - previousVal) / previousVal) * 100;
                let percentageComponent = percentIncrease > 0 ? '↑' + Math.abs(percentIncrease.toFixed(2)) : '↓' + Math.abs(percentIncrease.toFixed(2));
                let moreExpensive = curentVal - previousVal;

                $('#set-percentage-increase-value').html(percentageComponent);
                $('#set-more-expensive').html(moreExpensive.toLocaleString());

            }
        }

        let getTotalSales = function () {
            let variant = $("#variantMonthly").val().replace('string:', '');
            let variantArea = $("#variantAreaMonthly").val() ? $("#variantAreaMonthly").val().replace('string:', '') : '?';
            let startMonth = $("#startMonth").val().replace('string:', '');
            let endMonth = $("#endMonth").val().replace('string:', '');
            let yearList = $("#yearList").val().replace('string:', '');
            let vsComparison = $("input[name='monthlyYearComparison']:checked").val();

            let getParam = variant.trim() + '&' + variantArea + '&' + 'monthly' + '&' + startMonth + '&' + endMonth + '&' + yearList + '&' + vsComparison;

            this.currentTotalSales = currentTotalSales.query({
                currentTotalSales: getParam
            }, function (response) {
                if (response.length != 0) {
                    response.forEach(function (item) {
                        if (item._id.year == yearList) { // Current Year
                            let currentTotalSales = Math.ceil(item.count).toLocaleString();
                            $('#set-current-total-sales').html(currentTotalSales);
                        }
                    });
                } else {
                    $('#set-current-total-sales').html('0');
                }
            });
        };

        this.selectAreaPopulate = function ($scope) {

            let getParam = $scope;
            let selectValue = '';

            this.areas = areaDropdown.query({
                areaDropdown: getParam
            }, function (response) {
                response.forEach(function (item) {
                    selectValue += `<option label='${item}' value='${item}'>${item}</option>`;
                    $('.select-area-populate').html(selectValue);
                });
            });
        };

        this.findByCounty = function () {
            this.selections = this.opts;
            this.todo = findByCounty.query({
                findByCounty: $routeParams.findByCounty
            }, function (response) {
                hideLoader();
                if (response) {
                    if (typeof response[0] === 'undefined') {
                        dataGraphsDefault(response);
                        //$("#noResult").html('No Result');
                    } else {
                        $("#chartTitle").html(response[0].county);
                        dataGraphs(response);
                    }
                } else {
                    $("#noResult").html('No Result')
                }
            });

        };

        this.findOne = function () {
            this.todo = Todos.get({
                todoId: $routeParams.todoId
            });
        };

        let parseData = function (response) {
            return JSON.parse(response.csvData);
        };

        this.update = function () {
            this.todo.$update(function () {
                $location.path('todos/' + this.todo._id);
            }, function (errorResponse) {
                this.error = errorResponse.data.message;
            });
        };

        this.delete = function (todo) {
            if (todo) {
                todo.$remove(function () {
                    for (var i in this.todos) {
                        if (this.todos[i] === todo) {
                            this.todos.splice(i, 1);
                        }
                    }
                });
            } else {
                this.todo.$remove(function () {
                    $location.path('todos');
                });
            }
        };

    }
]);