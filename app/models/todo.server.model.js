var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var TodoSchema = new Schema({
    created: {
        type: Date
        //default: Date.now
    },
    title: {
        type: String,
        default: '',
        trim: true,
        required: "Title can't be blank"
    },
    csvData: {
        type: Object,
        default: '',
        trim: true
    },
    date: {
        type: Date,
        default: '',
        trim: true
    },
    address: {
        type: String,
        default: '',
        trim: true
    },
    area: {
        type: String,
        default: '',
        trim: true
    },
    postal: {
        type: String,
        default: '',
        trim: true
    },
    county: {
        type: String,
        default: '',
        trim: true
    },
    price: {
        type: Number,
        default: '',
        trim: true
    },
    creator: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    completed: {
        type: Boolean,
        default: false
    }
});
mongoose.model('Todo', TodoSchema);