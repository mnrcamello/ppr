/**
 * Created by pcc on 06-Dec-16.
 */
var mongoose = require('mongoose'),
    Todo = mongoose.model('Todo');

var getErrorMessage = function (err) {
    if (err.errors) {
        for (var errName in err.errors) {
            if (err.errors[errName].message) return err.errors[errName].message;
        }
    } else {
        return 'Unknown server error';
    }
};

exports.create = function (req, res) {
    var todo = new Todo(req.body);
    todo.creator = req.user;
    todo.save(function (err) {
        if (err) {
            return res.status(400).send({
                message: getErrorMessage(err)
            });
        } else {
            res.json(todo);
        }
    });
};

//Not using this anymore as it fetches all the todos from the database
exports.list = function (req, res) {
    Todo.find().sort('-created').populate('creator', 'name username').exec(function (err, todos) {
        if (err) {
            return res.status(400).send({
                message: getErrorMessage(err)
            });
        } else {
            res.json(todos);
        }
    });
};

//List only the todos created by the User currently logged in
exports.listCreatedTodos = function (req, res) {
    Todo.find({
        "creator": req.user._id
    }).sort('-created').populate('creator', 'name username').exec(function (err, todos) {
        if (err) {
            return res.status(400).send({
                message: getErrorMessage(err)
            });
        } else {
            res.json(todos);
        }
    });
};

exports.read = function (req, res) {
    res.json(req.todo);
};

exports.todoByID = function (req, res, next, id) {
    Todo.findById(id).populate('creator', 'name username').exec(function (err, todo) {
        if (err)
            return next(err);

        if (!todo)
            return next(new Error('Failed to load todo ' + id));

        req.todo = todo;
        next();
    });
};

exports.listAll = function (req, res) {
    setTimeout(function () {
        Todo.find().sort('-created').populate('creator', 'name username').exec(function (err, todos) {
            if (err) {
                return res.status(400).send({
                    message: getErrorMessage(err)
                });
            } else {
                res.json(todos);
            }
        });
    }, 1000);
};

exports.listDefault = function (req, res, next) {
    let countyVal = '';
    if (typeof todo === 'undefined') {
        countyVal = null;
    } else {
        countyVal = todo.county;
    }

    setTimeout(function () {
        Todo.findOne().populate('creator', 'name username').exec(function (err, todo) {
            Todo.find({
                county: countyVal,

            }).exec(function (err, todo) {
                if (err)
                    return next(err);

                if (!todo)
                    todo = '';

                res.json(todo);
            });
        });
    }, 1000);

};

exports.countyDropdown = function (req, res, date) {
    Todo.find({}).distinct('county', function (error, ids) {}).exec(function (err, todo) {
        if (err)
            return next(err);

        if (!todo)
            todo = '';

        res.json(todo);
    });

};

exports.highestLowestValue = function (req, res, next, date) {
    let search = date.split('&');
    if (search[0] === undefined) {
        res.json('');
    } else {
        if (search[2] == 'monthly') {
            let countyResult = search[0];
            let startMonth = search[3];
            let endMonth = search[4];
            let yearResult = search[5];
            let endYear = search[5];
            let startYear = search[5];
            let start_ = startYear + '-' + startMonth + '-01';
            let end_ = endYear + '-' + endMonth + '-31';

            var start = new Date(start_);
            var end = new Date(end_);

            if (search[1] != '?') {
                let areaResult = search[1];
                Todo.aggregate(
                    [{
                            $match: {
                                county: countyResult,
                                area: areaResult,
                                "date": {
                                    "$gte": start,
                                    "$lte": end
                                }
                            }
                        },
                        {
                            $group: {
                                _id: {
                                    "year": {
                                        "$year": "$date"
                                    }
                                },
                                minPrice: {
                                    $min: "$price"
                                },
                                maxPrice: {
                                    $max: "$price"
                                },
                                count: {
                                    $sum: 1
                                }
                            }
                        }
                    ]
                ).exec(function (err, todo) {
                    if (err)
                        return next(err);

                    if (!todo)
                        todo = '';

                    res.json(todo);
                });
            } else {
                Todo.aggregate(
                    [{
                            $match: {
                                county: countyResult,
                                "date": {
                                    "$gte": start,
                                    "$lte": end
                                }
                            }
                        },
                        {
                            $group: {
                                _id: {
                                    "year": {
                                        "$year": "$date"
                                    }
                                },
                                minPrice: {
                                    $min: "$price"
                                },
                                maxPrice: {
                                    $max: "$price"
                                },
                                count: {
                                    $sum: 1
                                }
                            }
                        }
                    ]
                ).exec(function (err, todo) {
                    if (err)
                        return next(err);

                    if (!todo)
                        todo = '';

                    res.json(todo);
                });
            }

        }
    }
};

exports.averageValue = function (req, res, next, date) {
    let search = date.split('&');
    if (search[0] === undefined) {
        res.json('');
    } else {
        if (search[2] == 'monthly') {
            let countyResult = search[0];
            let startMonth = search[3];
            let endMonth = search[4];
            let yearResult = search[5];
            let endYear = search[5];
            let startYear = search[5];
            let start_ = startYear + '-' + startMonth + '-01';
            let end_ = endYear + '-' + endMonth + '-31';

            var start = new Date(start_);
            var end = new Date(end_);

            if (search[1] != '?') {
                let areaResult = search[1];
                Todo.aggregate(
                    [{
                            $match: {
                                county: countyResult,
                                area: areaResult,
                                "date": {
                                    "$gte": start,
                                    "$lte": end
                                }
                            }
                        },
                        { $sort : { date : 1} },
                        {
                            $group: {
                                _id: {
                                    "year": {
                                        "$year": "$date"
                                    }
                                },
                                minPrice: {
                                    $min: "$price"
                                },
                                maxPrice: {
                                    $max: "$price"
                                },
                                count: {
                                    $sum: 1
                                }
                            }
                        }
                    ]
                ).exec(function (err, todo) {
                    if (err)
                        return next(err);

                    if (!todo)
                        todo = '';

                    res.json(todo);
                });
            } else {
                Todo.aggregate(
                    [{
                            $match: {
                                county: countyResult,
                                "date": {
                                    "$gte": start,
                                    "$lte": end
                                }
                            }
                        },
                        { $sort : { date : 1} },
                        {
                            $group: {
                                _id: {
                                    "year": {
                                        "$year": "$date"
                                    }
                                },
                                totalAmount: {
                                    $sum: "$price"
                                },
                                count: {
                                    $sum: 1
                                }
                            }
                        }
                    ]
                ).exec(function (err, todo) {
                    if (err)
                        return next(err);

                    if (!todo)
                        todo = '';

                    res.json(todo);
                });
            }
        }
    }
}

exports.currentTotalSales = function (req, res, next, date) {
    let search = date.split('&');
    if (search[0] === undefined) {
        res.json('');
    } else {
        if (search[2] == 'monthly') {
            let countyResult = search[0];
            let startMonth = search[3];
            let endMonth = search[4];
            let yearResult = search[5];
            let endYear = search[5];
            let startYear = search[5];
            let start_ = startYear + '-' + startMonth + '-01';
            let end_ = endYear + '-' + endMonth + '-31';

            var start = new Date(start_);
            var end = new Date(end_);

            if (search[1] != '?') {

                let areaResult = search[1];
                Todo.aggregate(
                    [{
                            $match: {
                                county: countyResult,
                                area: areaResult,
                                "date": {
                                    "$gte": start,
                                    "$lte": end
                                }
                            }
                        },
                        { $sort : { date : 1} },
                        {
                            $group: {
                                _id: {
                                    "year": {
                                        "$year": "$date"
                                    }
                                },
                                totalAmount: { $sum: "$price" },
                                count: { $sum: 1  }
                            }
                        }
                    ]
                ).exec(function (err, todo) {
                    if (err)
                        return next(err);

                    if (!todo)
                        todo = '';

                    res.json(todo);
                });
            } else {
                Todo.aggregate(
                    [{
                            $match: {
                                county: countyResult,
                                "date": {
                                    "$gte": start,
                                    "$lte": end
                                }
                            }
                        },
                        { $sort : { date : 1} },
                        {
                            $group: {
                                _id: {
                                    "year": {
                                        "$year": "$date"
                                    }
                                },
                                totalAmount: { $sum: "$price" },
                                count: { $sum: 1  }
                            }
                        }
                    ]
                ).exec(function (err, todo) {
                    if (err)
                        return next(err);

                    if (!todo)
                        todo = '';

                    res.json(todo);
                });
            }
        }
    }
}

exports.nationalMedian = function (req, res, date) {

    Todo.aggregate(
        [{
            $group: {
                _id: {
                    year: {
                        $year: "$date"
                    }
                },
                totalAmount: {
                    $sum: {
                        $multiply: ["$price"]
                    }
                },
                count: {
                    $sum: 1
                }
            }
        }]
    ).exec(function (err, todo) {
        if (err)
            return next(err);

        if (!todo)
            todo = '';

        res.json(todo);
    });

};

exports.areaDropdown = function (req, res, next, county) {
    let search = county;

    Todo.find({}).distinct('area', {
            "county": search
        },
        function (error, ids) {}).exec(function (err, todo) {
        if (err)
            return next(err);

        if (!todo)
            todo = '';

        res.json(todo);
    });

};

exports.findByCounty = function (req, res, next, date) {
    let search = date.split('&');
    if (search[0] === undefined) {
        res.json('');
    } else {
        if (search[2] == 'range') {
            if (search[1] != '?') {
                setTimeout(function () {
                    Todo.find({
                        county: search[0],
                        area: search[1],
                        "date": {
                            "$gte": search[3],
                            "$lte": search[4]
                        }
                    }).exec(function (err, todo) {
                        if (err)
                            return next(err);

                        if (!todo) {
                            todo = '';

                        }

                        res.json(todo);
                    });
                }, 1000);
            } else {
                setTimeout(function () {
                    Todo.find({
                        county: search[0],
                        "date": {
                            "$gte": search[3],
                            "$lte": search[4]
                        }
                    }).exec(function (err, todo) {
                        if (err)
                            return next(err);

                        if (!todo) {
                            todo = '';

                        }

                        res.json(todo);
                    });
                }, 1000);

            }

        }
        if (search[2] == 'monthly') {
            let countyResult = search[0];
            let startMonth = search[3];
            let endMonth = search[4];
            let yearResult = search[5];
            let vsYear = search[6];
            let endYear = search[5];
            let startYear = search[5] - vsYear;
            let start_ = startYear + '-' + startMonth + '-01';
            let end_ = endYear + '-' + endMonth + '-01';

            var start = new Date(start_);
            var end = new Date(end_);

            if (search[1] != '?') {
                let areaResult = search[1];
                setTimeout(function () {
                    Todo.aggregate(
                        [{
                                $match: {
                                    county: countyResult,
                                    area: areaResult,
                                    "date": {
                                        "$gte": start,
                                        "$lte": end
                                    }
                                }
                            },
                            {
                                $group: {
                                    _id: {
                                        "year": {
                                            "$year": "$date"
                                        },
                                        "month": {
                                            "$month": "$date"
                                        }
                                    },
                                    totalAmount: {
                                        $sum: "$price"
                                    },
                                    avgQuantity: {
                                        $avg: "$price"
                                    },
                                    count: {
                                        $sum: 1
                                    }
                                }
                            }
                        ]
                    ).exec(function (err, todo) {
                        if (err) {
                            console.log('Err');
                            return next(err);
                        }

                        if (!todo) {
                            todo = '';

                        }

                        res.json(todo);
                    });
                }, 1000);
            } else {
                setTimeout(function () {
                    Todo.aggregate(
                        [{
                                $match: {
                                    county: countyResult,
                                    "date": {
                                        "$gte": start,
                                        "$lte": end
                                    }
                                }
                            },
                            {
                                $group: {
                                    _id: {
                                        "year": {
                                            "$year": "$date"
                                        },
                                        "month": {
                                            "$month": "$date"
                                        }
                                    },
                                    totalAmount: {
                                        $sum: "$price"
                                    },
                                    avgQuantity: {
                                        $avg: "$price"
                                    },
                                    count: {
                                        $sum: 1
                                    }
                                }
                            }
                        ]
                    ).exec(function (err, todo) {
                        if (err) {
                            console.log('Err');
                            return next(err);
                        }

                        if (!todo) {
                            todo = '';

                        }

                        res.json(todo);
                    });
                }, 1000);
            }
        }
        if (search[2] == 'yearly') {
            let start_ = search[3] + '-01' + '-01';
            let end_ = search[4] + '-12' + '-01';
            var start = new Date(start_);
            var end = new Date(end_);

            if (search[1] != '?') {
                setTimeout(function () {
                    Todo.aggregate(
                        [{
                                $match: {
                                    county: search[0],
                                    area: search[1],
                                    "date": {
                                        "$gte": start,
                                        "$lte": end
                                    }
                                }
                            },
                            {
                                $group: {
                                    _id: {
                                        "year": {
                                            "$year": "$date"
                                        }
                                    },
                                    totalAmount: {
                                        $sum: "$price"
                                    },
                                    avgQuantity: {
                                        $avg: "$price"
                                    },
                                    count: {
                                        $sum: 1
                                    }
                                }
                            }
                        ]
                    ).exec(function (err, todo) {
                        if (err) {
                            console.log('Err');
                            return next(err);
                        }

                        if (!todo) {
                            todo = '';

                        }

                        res.json(todo);
                    });
                }, 1000);
            } else {
                setTimeout(function () {
                    Todo.aggregate(
                        [{
                                $match: {
                                    county: search[0],
                                    "date": {
                                        "$gte": start,
                                        "$lte": end
                                    }
                                }
                            },
                            {
                                $group: {
                                    _id: {
                                        "year": {
                                            "$year": "$date"
                                        }
                                    },
                                    totalAmount: {
                                        $sum: "$price"
                                    },
                                    avgQuantity: {
                                        $avg: "$price"
                                    },
                                    count: {
                                        $sum: 1
                                    }
                                }
                            }
                        ]
                    ).exec(function (err, todo) {
                        if (err) {
                            console.log('Err');
                            return next(err);
                        }

                        if (!todo) {
                            todo = '';

                        }

                        res.json(todo);
                    });
                }, 1000);
            }


        }
        if (search[2] == 'quarterly') {

            let vsYear = search[4];
            let endYear = search[3];
            let startYear = search[3] - vsYear;
            let start_ = startYear + '-01' + '-01';
            let end_ = endYear + '-12' + '-01';
            var start = new Date(start_);
            var end = new Date(end_);
            if (search[1] != '?') {
                setTimeout(function () {
                    Todo.aggregate(
                        [{
                                $match: {
                                    county: search[0],
                                    area: search[1],
                                    "date": {
                                        "$gte": start,
                                        "$lte": end
                                    }
                                }
                            },
                            {
                                $group: {
                                    _id: {
                                        "year": {
                                            "$year": "$date"
                                        },
                                        "month": {
                                            "$month": "$date"
                                        }
                                    },
                                    totalAmount: {
                                        $sum: "$price"
                                    },
                                    avgQuantity: {
                                        $avg: "$price"
                                    },
                                    count: {
                                        $sum: 1
                                    }
                                }
                            }
                        ]
                    ).exec(function (err, todo) {
                        if (err) {
                            return next(err);
                        }

                        if (!todo) {
                            todo = '';

                        }

                        res.json(todo);
                    });
                }, 1000);
            } else {
                setTimeout(function () {
                    Todo.aggregate(
                        [{
                                $match: {
                                    county: search[0],
                                    "date": {
                                        "$gte": start,
                                        "$lte": end
                                    }
                                }
                            },
                            {
                                $group: {
                                    _id: {
                                        "year": {
                                            "$year": "$date"
                                        },
                                        "month": {
                                            "$month": "$date"
                                        }
                                    },
                                    totalAmount: {
                                        $sum: "$price"
                                    },
                                    avgQuantity: {
                                        $avg: "$price"
                                    },
                                    count: {
                                        $sum: 1
                                    }
                                }
                            }
                        ]
                    ).exec(function (err, todo) {
                        if (err) {
                            console.log('Err');
                            return next(err);
                        }

                        if (!todo) {
                            todo = '';

                        }

                        res.json(todo);
                    });
                }, 1000);
            }

        }
    }
};

exports.update = function (req, res) {
    var todo = req.todo;
    todo.title = req.body.title;
    todo.csvData = req.body.csvData;
    todo.completed = req.body.completed;

    todo.save(function (err) {
        if (err) {
            return res.status(400).send({
                message: getErrorMessage(err)
            });
        } else {
            res.json(todo);
        }
    });
};

exports.delete = function (req, res) {
    var todo = req.todo;
    todo.remove(function (err) {
        if (err) {
            return res.status(400).send({
                message: getErrorMessage(err)
            });
        } else {
            res.json(todo);
        }
    });
};

exports.hasAuthorization = function (req, res, next) {
    if (req.todo.creator.id !== req.user.id) {
        return res.status(403).send({
            message: 'User is not authorized'
        });
    }
    next();
};