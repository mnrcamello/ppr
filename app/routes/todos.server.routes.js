var users = require('../../app/controllers/users.server.controller'),
    todos = require('../../app/controllers/todos.server.controller');

module.exports = function(app){
    app.route('/api/todos')
        //.get(todos.listCreatedTodos)
        .get(todos.listAll)
        .post(users.requiresLogin, todos.create);

    app.route('/api/todos/:todoId')
        .get(todos.read)
        .put(users.requiresLogin, todos.hasAuthorization, todos.update)
        .delete(users.requiresLogin, todos.hasAuthorization, todos.delete);

    /* app.route('/api/list')
        .get(todos.listDefault) */

    app.route('/api/list/:findByCounty')
        .get(todos.listAll)

        app.route('/api/county/:countyDropdown')
        .get(todos.listAll)

        app.route('/api/area/:areaDropdown')
        .get(todos.listAll)

        app.route('/api/national/:nationalMedian')
        .get(todos.listAll)

        app.route('/api/highestlowest/:highestLowestValue')
        .get(todos.listAll)

        app.route('/api/getaverage/:averageValue')
        .get(todos.listAll)

        app.route('/api/totalsales/:currentTotalSales')
        .get(todos.listAll)

    app.route('/api/list')
        .get(todos.listDefault)

    app.param('todoId', todos.todoByID);
    app.param('findByCounty', todos.findByCounty);
    app.param('countyDropdown', todos.countyDropdown);
    app.param('areaDropdown', todos.areaDropdown);
    app.param('nationalMedian', todos.nationalMedian);
    app.param('highestLowestValue', todos.highestLowestValue);
    app.param('averageValue', todos.averageValue);
    app.param('currentTotalSales', todos.currentTotalSales);
};
